package com.example.gsbgestiondescontacts;

public class Personne {
    private int id;
    private String nom;
    private String prenom;
    private String tel;
    private String codepostal;
    private String adresse;
    private String ville;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom.toUpperCase();
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        StringBuilder vPrenom = new StringBuilder(prenom.toLowerCase());
        char vPremier = Character.toUpperCase(prenom.charAt(0));
        vPrenom.setCharAt(0, vPremier);
        this.prenom = vPrenom.toString();
    }
    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCodePostal() {
        return codepostal;
    }
    public void setCodePostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }
    public void setVille(String ville) {
        this.ville = ville;
    }

    // Ne pas l’oublier afin de pouvoir afficher les valeurs dans une liste
    @Override
    public String toString() {
        return nom + ' ' + prenom + ' ' + tel + ' ' + codepostal + ' ' + adresse + ' ' + ville;
    }
}
