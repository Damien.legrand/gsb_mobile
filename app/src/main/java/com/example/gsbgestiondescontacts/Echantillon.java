package com.example.gsbgestiondescontacts;

public class Echantillon {

    private int id;
    private String nom;
    private String type;
    private String libelle;
    private String quantite;
    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom.toUpperCase();
    }


    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }


    public String getQuantite() {
        return quantite;
    }
    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    // Ne pas l’oublier afin de pouvoir afficher les valeurs dans une liste
    @Override
    public String toString() {
        return nom + ' ' + type + ' ' + libelle + ' ' + quantite + ' ' + date;
    }
}






