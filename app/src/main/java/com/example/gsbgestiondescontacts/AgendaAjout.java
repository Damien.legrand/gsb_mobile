package com.example.gsbgestiondescontacts;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
    public class AgendaAjout extends Activity {
    //Déclaration des 3 boutons
    Button btnEnregistrer = null;
    //Button btnModifier = null;
    Button btnRetour = null;
    //Déclaration des 3 zones de texte
    EditText txtNom = null;
    EditText txtPrenom = null;
    EditText txtTel = null;
    EditText txtCodePostal = null;
    EditText txtAdresse = null;
    EditText txtVille = null;

    private BDPersonnel vBD;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_ajout);
        vBD = new BDPersonnel(this);
        //EditText
        txtNom = findViewById(R.id.nomAgendaAjout);
        txtPrenom = findViewById(R.id.prenomAgendaAjout);
        txtTel = findViewById(R.id.telAgendaAjout);
        txtCodePostal = findViewById(R.id.CodePostalAgendaAjout);
        txtAdresse = findViewById(R.id.AdresseAgendaAjout);
        txtVille = findViewById(R.id.VilleAgendaAjout);

        //BTN
        btnEnregistrer = findViewById(R.id.enregistreAgendaAjout);
        btnEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                Personne vPersonne = new Personne();
                vPersonne.setNom(txtNom.getText().toString());
                vPersonne.setPrenom(txtPrenom.getText().toString());
                vPersonne.setTel(txtTel.getText().toString());
                vPersonne.setCodePostal(txtCodePostal.getText().toString());
                vPersonne.setAdresse(txtAdresse.getText().toString());
                vPersonne.setVille(txtVille.getText().toString());
                vBD.ajouter(vPersonne);
                procEfface();
            }
        });


            //Liaison de la variable avec le bouton d’effacement du fichier xml
        //btnModifier = findViewById(R.id.ModifierAgendaAjout);

        //Lie le bouton à l’événement qui exécutera la procédure écrite en dessous
        //btnModifier.setOnClickListener(btnModifierListener);

        //Liaison de la variable avec le bouton de retour du fichier xml
        btnRetour = (Button) findViewById(R.id.retourAgendaAjout);

        //Lie le bouton à l’événement
        btnRetour.setOnClickListener(new View.OnClickListener() {
            //Procédure à executer lors de l’appui sur le bouton btnRetour
            @Override
            public void onClick(View v) {
        // TODO Auto-generated method stub
        //Termine l’activité
                finish();
            }
        });


        }
        private void procEfface()
        {
            txtNom.getText().clear();
            txtPrenom.getText().clear();
            txtTel.getText().clear();
            txtCodePostal.getText().clear();
            txtAdresse.getText().clear();
            txtVille.getText().clear();
        }


    }