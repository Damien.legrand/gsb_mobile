package com.example.gsbgestiondescontacts;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.*;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import java.util.ArrayList;

public class BDEchantillons extends SQLiteOpenHelper {

    private SQLiteDatabase bd;
    public BDEchantillons(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
// TODO Auto-generated constructor stub
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // db.execSQL("DROP TABLE echantillons;");
// TODO Auto-generated method stub
        db.execSQL("CREATE TABLE echantillons ("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "nom_echantillons TEXT NOT NULL,"
                + "type TEXT NOT NULL,"
                + "libelle TEXT NOT NULL,"
                + "quantite TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// TODO Auto-generated method stub
        db.execSQL("DROP TABLE enchantillons");
        onCreate(db);
    }
    public BDEchantillons(Context ctx) {
        super(ctx, "echantillons.bd", null, 1);
        bd = getWritableDatabase();
    }
    public void fermeture() {
        bd.close();
    }
    public long ajouter(Echantillon echantillon) {
        ContentValues vValeurs = new ContentValues();
        vValeurs.put("nom_echantillons", echantillon.getNom());
        vValeurs.put("type", echantillon.getType());
        vValeurs.put("libelle", echantillon.getLibelle());
        vValeurs.put("quantite", echantillon.getQuantite());
        return bd.insert("echantillons", null, vValeurs);
    }

    public int miseAJour(Echantillon echantillon) {
        ContentValues vValeurs = new ContentValues();
        vValeurs.put("nom_echantillons", echantillon.getNom());
        vValeurs.put("type", echantillon.getType());
        vValeurs.put("libelle", echantillon.getLibelle());
        vValeurs.put("quantite", echantillon.getQuantite());
        return bd.update("echantillons", vValeurs, "id = " + echantillon.getId(), null);
    }
    public int supprimer(Echantillon echantillon) {
        return bd.delete("echantillons", "id = " + echantillon.getId(), null);
    }

    public Echantillon getEchantillon(int id) {
        String[] vColonnes = {"nom_echantillons", "type", "libelle"};
        Cursor vCurseur = bd.query("echantillons", vColonnes, "id = " + id, null, null, null,
                "nom_echantillons");
        if(vCurseur.getCount() == 0)
            return null;
        else
        {
            vCurseur.moveToFirst();
            Echantillon vEchantillon = new Echantillon();
            vEchantillon.setId(id);
            vEchantillon.setNom(vCurseur.getString(0));
            vEchantillon.setType(vCurseur.getString(1));
            vEchantillon.setLibelle(vCurseur.getString(2));
            vEchantillon.setQuantite(vCurseur.getString(3));
            vEchantillon.setDate(vCurseur.getString(4));
            vCurseur.close();
            return vEchantillon;
        }
    }
    public ArrayList<Echantillon> getEchantillon() {
        ArrayList<Echantillon> vListe = new ArrayList<Echantillon>();
        Cursor vCurseur = bd.query("echantillons", null, null, null, null, null, "nom_echantillons, type");
        if(vCurseur.getCount() == 0) return vListe;
        else
        {
            vCurseur.moveToFirst();
            do
            {
                vListe.add(curseurPourEchantillon(vCurseur));
            }
            while(vCurseur.moveToNext());
            vCurseur.close();
            return vListe;
        }
    }
    /* public void ouverture(Context ctx) {
    bd = new BDPersonnel(ctx, "personnels.bd", null, 1).getWritableDatabase();
    }*/
    public Echantillon curseurPourEchantillon(Cursor curseur) {
        Echantillon vEchantillon = new Echantillon();
        vEchantillon.setId(curseur.getInt(0));
        vEchantillon.setNom(curseur.getString(1));
        vEchantillon.setType(curseur.getString(2));
        vEchantillon.setLibelle(curseur.getString(3));
        vEchantillon.setQuantite(curseur.getString(4));
        vEchantillon.setDate(curseur.getString(5));
        return vEchantillon;
    }
}
