package com.example.gsbgestiondescontacts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {   // Crée le menu dans la feuille.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuGeneAgenda:
                Intent leMenuAgenda = new Intent(MainActivity.this, AgendaVoir.class);//Association del'indentifiant à notreintent
                startActivity(leMenuAgenda);
                break;
            case R.id.mnuGeneEchantillon:
               Intent leMenuEchantillon = new Intent(MainActivity.this, EchantillonVoir.class);//Association del'indentifiant à notreintent
                startActivity(leMenuEchantillon);
                break;
            case R.id.mnuQuitter:
                //Affichage d'un message
                Toast.makeText(this, "Fin de l'application.", Toast.LENGTH_SHORT).show();
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

