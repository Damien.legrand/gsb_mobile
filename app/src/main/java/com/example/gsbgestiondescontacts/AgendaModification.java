package com.example.gsbgestiondescontacts;

import android.app.*;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import java.util.ArrayList;
public class AgendaModification extends ListActivity {
    private BDPersonnel bd;
    private ArrayList<Personne> vPersonnes = new ArrayList<Personne>();
    private Button btnRetour = null, btnModifTout = null, btnModifNom = null,
            btnModifPrenom = null, btnModifTel = null, btnModifCodepostal = null, btnModifAdresse = null, btnModifVille = null;
    private Personne vPersonne;
    private EditText vNom = null, vPrenom = null, vTel = null, vCodePostal = null, vAdresse = null, vVille = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_modification);
        bd = new BDPersonnel(this);
        vNom = findViewById(R.id.nomAgendaModification);
        vPrenom = findViewById(R.id.prenomAgendaModification);
        vTel = findViewById(R.id.telAgendaModification);
        vCodePostal = findViewById(R.id.CodePostalAgendaModification);
        vAdresse = findViewById(R.id.AdresseAgendaModification);
        vVille = findViewById(R.id.VilleAgendaModification);

        btnRetour = findViewById(R.id.retourAgendaModification);
        btnRetour.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                finish();
            }
        });
        btnModifTout = findViewById(R.id.modifierAgendaModification);
        btnModifTout.setEnabled(false);
        btnModifTout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                vPersonne.setNom(vNom.getText().toString());
                vPersonne.setPrenom(vPrenom.getText().toString());
                vPersonne.setTel(vTel.getText().toString());
                bd.miseAJour(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifCodepostal.setEnabled(false);
                btnModifAdresse.setEnabled(false);
                btnModifVille.setEnabled(false);
                onStart();
                procEfface();
            }
        });
        btnModifNom = findViewById(R.id.modifNomAgenda);
        btnModifNom.setEnabled(false);
        btnModifNom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
//bd.supprimer(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifCodepostal.setEnabled(false);
                btnModifAdresse.setEnabled(false);
                btnModifVille.setEnabled(false);
                vPersonne.setNom(vNom.getText().toString());
                bd.miseAJour(vPersonne);
                onStart();
                procEfface();
            }
        });
        btnModifPrenom = findViewById(R.id.modifPrenomAgenda);
        btnModifPrenom.setEnabled(false);
        btnModifPrenom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
//bd.supprimer(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifCodepostal.setEnabled(false);
                btnModifAdresse.setEnabled(false);
                btnModifVille.setEnabled(false);
                vPersonne.setPrenom(vPrenom.getText().toString());
                bd.miseAJour(vPersonne);
                onStart();
                procEfface();
            }
        });
        btnModifTel = findViewById(R.id.modifTelAgenda);
        btnModifTel.setEnabled(false);
        btnModifTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
//bd.supprimer(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifCodepostal.setEnabled(false);
                btnModifAdresse.setEnabled(false);
                btnModifVille.setEnabled(false);
                vPersonne.setTel(vTel.getText().toString());
                bd.miseAJour(vPersonne);
                onStart();
                procEfface();
            }
        });


        btnModifCodepostal = findViewById(R.id.modifCodepostalAgenda);
        btnModifCodepostal.setEnabled(false);
        btnModifCodepostal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
//bd.supprimer(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifAdresse.setEnabled(false);
                btnModifVille.setEnabled(false);
                vPersonne.setCodePostal(vCodePostal.getText().toString());
                bd.miseAJour(vPersonne);
                onStart();
                procEfface();
            }
        });

        btnModifAdresse = findViewById(R.id.modifAdresseAgenda);
        btnModifAdresse.setEnabled(false);
        btnModifAdresse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
//bd.supprimer(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifCodepostal.setEnabled(false);
                btnModifVille.setEnabled(false);
                vPersonne.setAdresse(vAdresse.getText().toString());
                bd.miseAJour(vPersonne);
                onStart();
                procEfface();
            }
        });

        btnModifVille = findViewById(R.id.modifVilleAgenda);
        btnModifVille.setEnabled(false);
        btnModifVille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
//bd.supprimer(vPersonne);
                btnModifTout.setEnabled(false);
                btnModifNom.setEnabled(false);
                btnModifPrenom.setEnabled(false);
                btnModifTel.setEnabled(false);
                btnModifCodepostal.setEnabled(false);
                btnModifAdresse.setEnabled(false);
                vPersonne.setVille(vVille.getText().toString());
                bd.miseAJour(vPersonne);
                onStart();
                procEfface();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        vPersonnes=bd.getPersonnes();
        setListAdapter(new ArrayAdapter<Personne>(this,
                android.R.layout.simple_list_item_1, vPersonnes));
    }
    public void onDestroy() {
        bd.fermeture();
        super.onDestroy();
    }
    @Override
    protected void onListItemClick(ListView liste, View vue, int position, long id) {
        vPersonne = vPersonnes.get(position);
        vNom.setText(vPersonne.getNom());
        vPrenom.setText(vPersonne.getPrenom());
        vTel.setText(vPersonne.getTel());
        vCodePostal.setText(vPersonne.getCodePostal());
        vAdresse.setText(vPersonne.getAdresse());
        vVille.setText(vPersonne.getVille());

        btnModifTout.setEnabled(true);
        btnModifNom.setEnabled(true);
        btnModifPrenom.setEnabled(true);
        btnModifTel.setEnabled(true);
        btnModifCodepostal.setEnabled(true);
        btnModifAdresse.setEnabled(true);
        btnModifVille.setEnabled(true);
    }

    private void procEfface()
    {
        vNom.getText().clear();
        vPrenom.getText().clear();
        vTel.getText().clear();
        vCodePostal.getText().clear();
        vAdresse.getText().clear();
        vVille.getText().clear();
    }

}