package com.example.gsbgestiondescontacts;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;

public class EchantillonVoir extends ListActivity {
    private BDEchantillons bd;
    private ArrayList<Echantillon> vEchantillon = new ArrayList<Echantillon>();
    private Button btnRetour = null;
    private Button btn_ajouter = null;
    private Button btn_modifier= null;
    private Button btn_supprimer = null;
    @Override  public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_echantillon_visualisation);
        bd = new BDEchantillons(this);

        btnRetour = (Button)findViewById(R.id.retourEchantillonVoir);
        btnRetour.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        btn_ajouter = findViewById(R.id.btn_ajout);
        btn_ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent AjouterEchantillons = new Intent(EchantillonVoir.this, EchantillonAjout.class);//Association del'indentifiant à notreintent
                startActivity(AjouterEchantillons);
            }
        });
        btn_modifier = findViewById(R.id.btn_modifier);
        btn_modifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ModifierEchantillon = new Intent(EchantillonVoir.this, EchantillonModification.class);//Association del'indentifiant à notreintent
                startActivity(ModifierEchantillon);
            }
        });

        btn_supprimer = findViewById(R.id.btn_supprimer);
        btn_supprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SupprimerEchantillon = new Intent(EchantillonVoir.this, EchantillonSuppression.class);//Association del'indentifiant à notreintent
                startActivity(SupprimerEchantillon);
            }
        });

    }    @Override
    protected void onStart() {
        super.onStart();
        vEchantillon=bd.getEchantillon();
        setListAdapter(new ArrayAdapter<Echantillon>(this, android.R.layout.simple_list_item_1, vEchantillon));
    }
    public void onDestroy() {
        bd.fermeture();
        super.onDestroy();
    }

}


