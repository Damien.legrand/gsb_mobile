package com.example.gsbgestiondescontacts;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.*;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import java.util.ArrayList;
public class BDPersonnel extends SQLiteOpenHelper {
    private SQLiteDatabase bd;
    public BDPersonnel(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
// TODO Auto-generated constructor stub
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
       // db.execSQL("DROP TABLE personnels;");
// TODO Auto-generated method stub
        db.execSQL("CREATE TABLE personnels ("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "nom TEXT NOT NULL,"
                + "prenom TEXT NOT NULL,"
                + "tel TEXT NOT NULL,"
                + "codepostal TEXT NOT NULL," +
                "  adresse TEXT NOT NULL," +
                "  ville TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// TODO Auto-generated method stub
        db.execSQL("DROP TABLE personnels");
        onCreate(db);
    }
    public BDPersonnel(Context ctx) {
        super(ctx, "personnels.bd", null, 1);
        bd = getWritableDatabase();
    }
    public void fermeture() {
        bd.close();
    }
    public long ajouter(Personne personne) {
        ContentValues vValeurs = new ContentValues();
        vValeurs.put("nom", personne.getNom());
        vValeurs.put("prenom", personne.getPrenom());
        vValeurs.put("tel", personne.getTel());
        vValeurs.put("codepostal", personne.getCodePostal());
        vValeurs.put("adresse", personne.getAdresse());
        vValeurs.put("ville", personne.getVille());
        return bd.insert("personnels", null, vValeurs);
    }
    public int miseAJour(Personne personne) {
        ContentValues vValeurs = new ContentValues();
        vValeurs.put("nom", personne.getNom());
        vValeurs.put("prenom", personne.getPrenom());
        vValeurs.put("tel", personne.getTel());
        vValeurs.put("codepostal", personne.getCodePostal());
        vValeurs.put("adresse", personne.getAdresse());
        vValeurs.put("ville", personne.getVille());
        return bd.update("personnels", vValeurs, "id = " + personne.getId(), null);
    }
    public int supprimer(Personne personne) {
        return bd.delete("personnels", "id = " + personne.getId(), null);
    }

    public Personne getPersonne(int id) {
        String[] vColonnes = {"nom", "prenom", "tel"};
        Cursor vCurseur = bd.query("personnels", vColonnes, "id = " + id, null, null, null,
                "nom, prenom");
        if(vCurseur.getCount() == 0)
            return null;
        else
        {
            vCurseur.moveToFirst();
            Personne vPersonne = new Personne();
            vPersonne.setId(id);
            vPersonne.setNom(vCurseur.getString(0));
            vPersonne.setPrenom(vCurseur.getString(1));
            vPersonne.setTel(vCurseur.getString(2));
            vPersonne.setCodePostal(vCurseur.getString(3));
            vPersonne.setAdresse(vCurseur.getString(4));
            vPersonne.setVille(vCurseur.getString(5));
            vCurseur.close();
            return vPersonne;
        }
    }
    public ArrayList<Personne> getPersonnes() {
        ArrayList<Personne> vListe = new ArrayList<Personne>();
        Cursor vCurseur = bd.query("personnels", null, null, null, null, null, "nom, prenom");
        if(vCurseur.getCount() == 0) return vListe;
        else
        {
            vCurseur.moveToFirst();
            do
            {
                vListe.add(curseurPourPersonne(vCurseur));
            }
            while(vCurseur.moveToNext());
            vCurseur.close();
            return vListe;
        }
    }
    /* public void ouverture(Context ctx) {
    bd = new BDPersonnel(ctx, "personnels.bd", null, 1).getWritableDatabase();
    }*/
    public Personne curseurPourPersonne(Cursor curseur) {
        Personne vPersonne = new Personne();
        vPersonne.setId(curseur.getInt(0));
        vPersonne.setNom(curseur.getString(1));
        vPersonne.setPrenom(curseur.getString(2));
        vPersonne.setTel(curseur.getString(3));
        vPersonne.setCodePostal(curseur.getString(4));
        vPersonne.setAdresse(curseur.getString(5));
        vPersonne.setVille(curseur.getString(6));
        return vPersonne;
    }
}