package com.example.gsbgestiondescontacts;

import android.app.*;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import java.util.ArrayList;
public class AgendaSuppression extends ListActivity {
    private BDPersonnel bd;
    private ArrayList<Personne> vPersonnes = new ArrayList<Personne>();
    private Button btnRetour = null, btnSupp = null;
    private Personne vPersonne;
    EditText vNom = null, vPrenom = null, vTel = null, vCodepostal = null, vAdresse = null, vVille = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_supprimer);
        bd = new BDPersonnel(this);
        btnRetour = (Button)findViewById(R.id.retourAgendaSuppression);
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                finish();
            }
        });
        btnSupp = (Button)findViewById(R.id.supprimerAgendaSuppression);
        btnSupp.setEnabled(false);
        btnSupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                bd.supprimer(vPersonne);
                btnSupp.setEnabled(false);
                onStart();
                procEfface();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        vPersonnes=bd.getPersonnes();
        setListAdapter(new ArrayAdapter<Personne>(this,
                android.R.layout.simple_list_item_1, vPersonnes));
    }
    public void onDestroy() {
        bd.fermeture();
        super.onDestroy();
    }
    @Override
    protected void onListItemClick(ListView liste, View vue, int position, long id) {

        //EditText
        vNom = findViewById(R.id.nomAgendaSuppression);
        vPrenom = findViewById(R.id.prenomAgendaSuppression);
        vTel = findViewById(R.id.telAgendaSuppression);
        vCodepostal = findViewById(R.id.CodePostalAgendaSuppression);
        vAdresse = findViewById(R.id.AdresseAgendaSuppression);
        vVille = findViewById(R.id.VilleAgendaSuppression);

        vPersonne = vPersonnes.get(position);
        vNom.setText(vPersonne.getNom());
        vPrenom.setText(vPersonne.getPrenom());
        vTel.setText(vPersonne.getTel());
        vCodepostal.setText(vPersonne.getCodePostal());
        vAdresse.setText(vPersonne.getAdresse());
        vVille.setText(vPersonne.getVille());
        btnSupp.setEnabled(true);

    }

    private void procEfface()
    {
        vNom.getText().clear();
        vPrenom.getText().clear();
        vTel.getText().clear();
        vCodepostal.getText().clear();
        vAdresse.getText().clear();
        vVille.getText().clear();
    }
}