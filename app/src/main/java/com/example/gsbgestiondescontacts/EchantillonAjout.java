package com.example.gsbgestiondescontacts;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EchantillonAjout extends Activity {
    //Déclaration des 3 boutons
    Button btnEnregistrer = null;
    //Button btnModifier = null;
    Button btnRetour = null;
    //Déclaration des 3 zones de texte
    EditText txtNom = null;
    EditText txtType = null;
    EditText txtLibelle = null;
    EditText txtQuantite = null;
    EditText txtDate = null;

    private BDEchantillons vBD;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_echantillon_ajout);
        vBD = new BDEchantillons(this);
        //EditText
        txtNom = findViewById(R.id.nomEchantillonAjout);
        txtType = findViewById(R.id.typeEchantillonAjout);
        txtLibelle = findViewById(R.id.libelleEchantillonAjout);
        txtQuantite = findViewById(R.id.quantiteEchantillonAjout);
        txtDate = findViewById(R.id.dateEchantillonAjout);

        //BTN
        btnEnregistrer = findViewById(R.id.enregistreAgendaAjout);
        btnEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                Echantillon vEchantillon = new Echantillon();
                vEchantillon.setNom(txtNom.getText().toString());
                vEchantillon.setType(txtType.getText().toString());
                vEchantillon.setLibelle(txtLibelle.getText().toString());
                vEchantillon.setQuantite(txtQuantite.getText().toString());
                vEchantillon.setDate(txtDate.getText().toString());
                vBD.ajouter(vEchantillon);
                procEfface();
            }
        });


        //Liaison de la variable avec le bouton d’effacement du fichier xml
        //btnModifier = findViewById(R.id.ModifierAgendaAjout);

        //Lie le bouton à l’événement qui exécutera la procédure écrite en dessous
        //btnModifier.setOnClickListener(btnModifierListener);

        //Liaison de la variable avec le bouton de retour du fichier xml
        btnRetour = findViewById(R.id.retourEchantillonAjout);

        //Lie le bouton à l’événement
        btnRetour.setOnClickListener(new View.OnClickListener() {
            //Procédure à executer lors de l’appui sur le bouton btnRetour
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Termine l’activité
                finish();
            }
        });


    }
    private void procEfface()
    {
        txtNom.getText().clear();
        txtType.getText().clear();
        txtLibelle.getText().clear();
        txtQuantite.getText().clear();
        txtDate.getText().clear();
    }


}