package com.example.gsbgestiondescontacts;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

// ListActivity permet de gérer automatiquement la liste
public class AgendaVoir extends ListActivity {
    private BDPersonnel bd;
    private ArrayList<Personne> vPersonnes = new ArrayList<Personne>();
    private Button btnRetour = null;
    private Button btn_ajouter = null;
    private Button btn_modifier= null;
    private Button btn_supprimer = null;
    @Override  public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);   setContentView(R.layout.activity_agenda_visualisation);
        bd = new BDPersonnel(this);

  btnRetour = (Button)findViewById(R.id.retourAgendaVoir);   btnRetour.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
  btn_ajouter = findViewById(R.id.btn_ajout);
  btn_ajouter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          Intent AjouterAgenda = new Intent(AgendaVoir.this, AgendaAjout.class);//Association del'indentifiant à notreintent
          startActivity(AjouterAgenda);
      }
  });
        btn_modifier = findViewById(R.id.btn_modifier);
        btn_modifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ModifierAgenda = new Intent(AgendaVoir.this, AgendaModification.class);//Association del'indentifiant à notreintent
                startActivity(ModifierAgenda);
            }
        });

        btn_supprimer = findViewById(R.id.btn_supprimer);
        btn_supprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SupprimerAgenda = new Intent(AgendaVoir.this, AgendaSuppression.class);//Association del'indentifiant à notreintent
                startActivity(SupprimerAgenda);
            }
        });

    }    @Override
    protected void onStart() {
        super.onStart();
        vPersonnes=bd.getPersonnes();
        setListAdapter(new ArrayAdapter<Personne>(this, android.R.layout.simple_list_item_1, vPersonnes));
    }
    public void onDestroy() {
        bd.fermeture();
        super.onDestroy();
    }
}